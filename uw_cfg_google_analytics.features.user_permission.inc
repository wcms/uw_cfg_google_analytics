<?php

/**
 * @file
 * uw_cfg_google_analytics.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_cfg_google_analytics_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer universal analytics'.
  $permissions['administer universal analytics'] = array(
    'name' => 'administer universal analytics',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'uw_cfg_google_analytics',
  );

  return $permissions;
}
