<?php

/**
 * @file
 * uw_cfg_google_analytics.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_cfg_google_analytics_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-management_google-universal-analytics:admin/config/system/uw_cfg_google_analytics_settings.
  $menu_links['menu-site-management_google-universal-analytics:admin/config/system/uw_cfg_google_analytics_settings'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/uw_cfg_google_analytics_settings',
    'router_path' => 'admin/config/system/uw_cfg_google_analytics_settings',
    'link_title' => 'Google Universal Analytics',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_google-universal-analytics:admin/config/system/uw_cfg_google_analytics_settings',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Google Universal Analytics');

  return $menu_links;
}
