<?php

/**
 * @file
 * uw_cfg_google_analytics.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_cfg_google_analytics_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'googleanalytics_pages';
  $strongarm->value = 'admin
admin/*
batch
node/add*';
  $export['googleanalytics_pages'] = $strongarm;

  return $export;
}
